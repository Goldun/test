FROM --platform=linux/amd64 valgoldun/tester
ENV PATH="/.venv/bin:$PATH"

COPY example-service-test .

CMD pipenv run pytest -v -p no:warnings --alluredir=/app/allure-results; /app/send_results.sh
